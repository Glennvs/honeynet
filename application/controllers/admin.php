<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc Determines if the user is an admin or not and displays the right view
 * @author Glenn Van Schil
 */
class Admin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Checks if user is admin
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    public function index()
    {
        $this->globalTest();
        $this->test();

        $data          = $this->getData();
        $data['honeylist'] = $this->honeyList();
        $data['users'] = $this->getUsername();
        if ($this->session->userdata('logged_in'))
        {
            $sess_array = $this->session->userdata('logged_in');
            if ($sess_array['admin'] != 0)
            {
                $session_data = $this->session->userdata('logged_in');
                $this->load->view('view_header', $data);
                $this->load->view('view_admin', $data);
                $this->load->view('view_footer');
            } //$sess_array['admin'] != 0
            else
            {
                $session_data = $this->session->userdata('logged_in');
                $this->load->view('view_header', $data);
                $this->load->view('view_error', $data);
                $this->load->view('view_footer');
            }
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }
    }

    /**
     * @desc Loads all the users in the database
     * @param none
     * @return array - contains the strings
     * @author Glenn Van Schil
     */
    function getUsername()
    {
        $result = $this->model_user->showUsers();

        $users = array();
        if (!empty($result))
        {
            foreach ($result as $row)
            {
                array_push($users, array(
                    $row->username,
                    $row->admin
                ));
            } //$result as $row

            return $users;
        } //!empty($result)
    }

    /**
     * @desc Test the different !!TESTABLE!! functions
     * @param none
     * @return none
     * @author Glenn Van Schil
     */
    function test()
    {
        $testData = array();

        //Test getUsername()
        $test           = $this->getUsername();
        $expectedResult = 'is_array';
        $testName       = "Test getUsername";
        $notes          = "Checks if getUsername() returns an array of data";

        $result = $this->unit->run($test, $expectedResult, $testName, $notes);

        array_push($testData, $result);

        $uri = 'test/controllerTests/test_admin.html';
        write_file($uri, '<h1>Test Results admin</h1>');
        write_file($uri, $this->unit->report(), 'a');
    }
}
?>
