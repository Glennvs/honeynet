<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc Show all the data retrieved from the attack attempts
 * @author Glenn Van Schil
 */
class Attack extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Puts the attackers details in an Array
     * @param none
     * @return loads the requested view
     * @author Glenn Van Schil
     */
    function index()
    {
        $result = $this->model_attack->loadAttacks();
        $data = $this->getData();
        $data['attack'] = array();

        foreach ($result as $row)
        {
            array_push($data['attack'], array(
                'session' => $row->session,
                'success' => $row->success,
                'username' => $row->username,
                'password' => $row->password,
                'timestamp' => $row->timestamp
            ));
        } //$result as $row

        $data['honeylist'] = $this->honeyList();

        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_attackKippo', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }

    }

    /**
     * @desc Puts the attackers details in an Array
     * @param none
     * @return loads the requested view
     * @author Glenn Van Schil
     */
    function labrea()
    {
        $result = $this->model_attack->loadAttacksLabrea();
        $data = $this->getData();
        $data['attack'] = array();

        foreach ($result as $row)
        {
            array_push($data['attack'], array(
                'receiver' => $row->ipontvanger,
                'sender' => $row->ipzender,
                'message' => $row->message,
                'date' => $row->date
            ));
        } //$result as $row

        $data['honeylist'] = $this->honeyList();

        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_attackLabrea', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }

    }
}
?>
