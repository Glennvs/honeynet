<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc Displays all the commands executer grouped by Session ID's
 * @author Glenn Van Schil
 */
class Command extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Retrievs a certain session and displays the view
     * @param $session - the session ID
     * @return loads the requested view
     * @author Glenn Van Schil
     */
    function getCommands($session = NULL)
    {
        $result = $this->model_command->loadCommands();
        $data = $this->getData();
        $data['command'] = array();
        $data['input']   = array();

        foreach ($result as $row)
        {
            array_push($data['command'], array(
                'session' => $row->session
            ));
        } //$result as $row

        if ($session !== '' && $session !== null)
        {
            $result = $this->model_command->loadDetails($session);
            foreach ($result as $row)
            {
                array_push($data['input'], array(
                    $row['timestamp'],
                    $row['input']
                ));
            } //$result as $row
        }
        $data['honeylist'] = $this->honeyList();
        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_commandsKippo', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }
    }
}
?>
