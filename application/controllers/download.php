<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc Retrievs the download files, places them in the website folder and puts them in the portal so users can download them
 * @author Glenn Van Schil
 * @author Lennart Pollaris
 */
class Download extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     * @desc Retrievs all the download files
     * @param none
     * @return loads the requested view
     * @author Glenn Van Schil
     */
    function index()
    {
        $result = $this->model_download->loadDownloads();
        $data = $this->getData();
        $data['download'] = array();
        $data['files']    = array();

        foreach ($result as $row)
        {
            //$file = $this->strReplaceLast("_", ".", $row->outfile);
            $file = substr($row->outfile, 3);
            $file = ltrim($file);
            array_push($data['files'], $file);
        }

        foreach ($result as $row)
        {
            array_push($data['download'], array(
                $row->session,
                $row->timestamp,
                $row->url
            ));
        } //$result as $row

        /*foreach ($result as $row)
        {
            $this->createFile($row->outfile);
        }*/

        $data['honeylist'] = $this->honeyList();
        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_downloadKippo', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }

    }

    /**
     * @desc checks if the file already exist, if not create it within the downloads folder
     * @param $url - location for file
     * @return none
     * @author Glenn Van Schil
     */
    /*function createFile($url)
    {
        $file = substr($url, 2);
        $name = $this->strReplaceLast("_", ".", $file);
        $name = ltrim($name);
        if (!file_exists("serverDownload" . $file))
        {
            file_put_contents("serverDownload" . $name, fopen("http://192.168.2.142" . $file, 'r'));
        }
    }*/

    /**
     * @desc Replace the last occurrence of a string.
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     * @author Glenn Van Schil
     */
    function strReplaceLast($search, $replace, $subject)
    {

        $lenOfSearch = strlen($search);
        $posOfSearch = strrpos($subject, $search);

        return substr_replace($subject, $replace, $posOfSearch, $lenOfSearch);

    }
}
?>
