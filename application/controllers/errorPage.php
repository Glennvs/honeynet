<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc When the user navigates to a restricted page or a page that doesn't exist he will be redirected to this page
 * @author Glenn Van Schil
 */
class ErrorPage extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Checks if user is admin
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    public function index()
    {
        $data = $this->getData();
        $data['honeylist'] = $this->honeyList();
        $this->load->view('view_header', $data);
        $this->load->view('view_error', $data);
        $this->load->view('view_footer');
    }
}
?>
