<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc Retrievs all the data needed to display the history data
 * @author Glenn Van Schil
 * @author Lennart Pollaris
 */
class History extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Loads the History data
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
        $data = $this->getData();
        $month = $this->model_history->lastMonth();
        $three = $this->model_history->lastThree();
        $two   = $this->model_history->lastTwo();
        $week  = $this->model_history->lastWeek();

        $data['month'] = array();
        $data['three'] = array();
        $data['two']   = array();
        $data['week']  = array();

        if (!empty($month))
            foreach ($month as $row)
            {
                array_push($data['month'], array(
                    $row->datum,
                    $row->ssh,
                    $row->php,
                    $row->malware
                ));
            } //$result as $row

        if (!empty($three))
            foreach ($three as $row)
            {
                array_push($data['three'], array(
                    $row->datum,
                    $row->ssh,
                    $row->php,
                    $row->malware
                ));
            } //$result as $row

        if (!empty($two))
            foreach ($two as $row)
            {
                array_push($data['two'], array(
                    $row->datum,
                    $row->ssh,
                    $row->php,
                    $row->malware
                ));
            } //$result as $row

        if (!empty($week))
            foreach ($week as $row)
            {
                array_push($data['week'], array(
                    $row->datum,
                    $row->ssh,
                    $row->php,
                    $row->malware
                ));
            } //$result as $row

        $data['honeylist'] = $this->honeyList();
        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_history', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }

    }
}
?>
