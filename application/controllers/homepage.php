<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc Navigation to the homepage or destroying the session on logout
 * @author Glenn Van Schil
 */
session_start();
class Homepage extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc opens the portal
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
        $data = $this->getData();
        $data['honeylist'] = $this->honeyList();

        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_home', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }

    }


    /**
     * @desc Show the changeable settings of the honneypot
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function settings()
    {
        $data = $this->getData();
        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_settings', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            //If no session, redirect to login page
            redirect('main', 'refresh');
        }
    }

    /**
     * @desc Logs the current user out and destroys the session
     * @param none
     * @return Loads the login panel
     * @author Glenn Van Schil
     */
    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('main', 'refresh');
    }
}
?>
