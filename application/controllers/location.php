<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc Retrievs all the data for displaying the donut chart and the google map
 * @author Glenn Van Schil
 */
class Location extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Loads all the IP information
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {

        $this->test();
        $data   = $this->getData();
        $data['map']     = $this->fillMap();
        $data['country'] = $this->fillGeo();
        $data['type']    = $this->donutTypes();
        $data['attacks']    = $this->donutAttacks();
        $data['honeylist'] = $this->honeyList();

        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_map', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            //If no session, redirect to login page
            redirect('main', 'refresh');
        }

    }

    /**
     * @desc Gathers all the data for the Visual map
     * @param none
     * @return Array - ip, longitude, latitude
     * @author Glenn Van Schil
     */
    function fillMap()
    {
        $result = $this->model_location->loadIps();
        $ipData = array();

        foreach ($result as $row)
        {
            array_push($ipData, array(
                'ip' => $row->ip,
                'geo1' => $row->geo1,
                'geo2' => $row->geo2
            ));
        }

        return $ipData;
    }

    /**
     * @desc Gathers all the data for the Donutchart
     * @param none
     * @return Array - ip, longitude, latitude
     * @author Glenn Van Schil
     */
    function fillGeo()
    {
        $result  = $this->model_location->loadIps();
        $country = array();

        foreach ($result as $row)
        {
            array_push($country, $row->land);
        }

        $country = $this->myCodes($country, 'long');
        $country = array_count_values($country);
        arsort($country);
        return $country;
    }

    /**
     * @desc Counts the different attacks in a country
     * @param none
     * @return Array of Country, type of attack, amount of this attack
     * @author Glenn Van Schil
     */
    function donutTypes()
    {
        $result = $this->model_location->groupTypes();
        $types  = array();

        foreach ($result as $row)
        {
            array_push($types, array(
                $this->myCodes($row->land, 'long'),
                $row->type,
                $row->aantal
            ));
        }

        return $types;
    }

    /**
     * @desc Counts the different attacks
     * @param none
     * @return Array of type of attack, amount of this attack
     * @author Glenn Van Schil
     */
    function donutAttacks()
    {
        $result = $this->model_location->groupAttacks();
        $attacks  = array();

        foreach ($result as $row)
        {
            array_push($attacks, array(
                $row->type,
                $row->aantal
            ));
        }

        return $attacks;
    }

    /**
     * @desc Converts country names or iso codes
     * @param $in - country name or country iso code
     * @param $type - the output format, long = country name, short = country ISO code
     * @return string - contains the requested code or name
     * @author Glenn Van Schil
     */
    function myCodes($in, $type)
    {
        $out   = "";
        $long  = array(
            'Afghanistan',
            'Åland Islands',
            'Albania',
            'Algeria',
            'American Samoa',
            'Andorra',
            'Angola',
            'Anguilla',
            'Antarctica',
            'Antigua and Barbuda',
            'Argentina',
            'Armenia',
            'Aruba',
            'Australia',
            'Austria',
            'Azerbaijan',
            'Bahamas',
            'Bahrain',
            'Bangladesh',
            'Barbados',
            'Belarus',
            'Belgium',
            'Belize',
            'Benin',
            'Bermuda',
            'Bhutan',
            'Bolivia - Plurinational State of',
            'Bonaire - Sint Eustatius and Saba',
            'Bosnia and Herzegovina',
            'Botswana',
            'Bouvet Island',
            'Brazil',
            'British Indian Ocean Territory',
            'Brunei Darussalam',
            'Bulgaria',
            'Burkina Faso',
            'Burundi',
            'Cambodia',
            'Cameroon',
            'Canada',
            'Cape Verde',
            'Cayman Islands',
            'Central African Republic',
            'Chad',
            'Chile',
            'China',
            'Christmas Island',
            'Cocos (Keeling) Islands',
            'Colombia',
            'Comoros',
            'Congo',
            'Congo - the Democratic Republic of the',
            'Cook Islands',
            'Costa Rica',
            'Côte d\'Ivoire',
            'Croatia',
            'Cuba',
            'Curaçao',
            'Cyprus',
            'Czech Republic',
            'Denmark',
            'Djibouti',
            'Dominica',
            'Dominican Republic',
            'Ecuador',
            'Egypt',
            'El Salvador',
            'Equatorial Guinea',
            'Eritrea',
            'Estonia',
            'Ethiopia',
            'Falkland Islands (Malvinas)',
            'Faroe Islands',
            'Fiji',
            'Finland',
            'France',
            'French Guiana',
            'French Polynesia',
            'French Southern Territories',
            'Gabon',
            'Gambia',
            'Georgia',
            'Germany',
            'Ghana',
            'Gibraltar',
            'Greece',
            'Greenland',
            'Grenada',
            'Guadeloupe',
            'Guam',
            'Guatemala',
            'Guernsey',
            'Guinea',
            'Guinea-Bissau',
            'Guyana',
            'Haiti',
            'Heard Island and McDonald Islands',
            'Holy See (Vatican City State)',
            'Honduras',
            'Hong Kong',
            'Hungary',
            'Iceland',
            'India',
            'Indonesia',
            'Iran - Islamic Republic of',
            'Iraq',
            'Ireland',
            'Isle of Man',
            'Israel',
            'Italy',
            'Jamaica',
            'Japan',
            'Jersey',
            'Jordan',
            'Kazakhstan',
            'Kenya',
            'Kiribati',
            'Korea - Democratic People\'s Republic of',
            'Korea - Republic of',
            'Kuwait',
            'Kyrgyzstan',
            'Lao People\'s Democratic Republic',
            'Latvia',
            'Lebanon',
            'Lesotho',
            'Liberia',
            'Libya',
            'Liechtenstein',
            'Lithuania',
            'Luxembourg',
            'Macao',
            'Macedonia - the former Yugoslav Republic of',
            'Madagascar',
            'Malawi',
            'Malaysia',
            'Maldives',
            'Mali',
            'Malta',
            'Marshall Islands',
            'Martinique',
            'Mauritania',
            'Mauritius',
            'Mayotte',
            'Mexico',
            'Micronesia - Federated States of',
            'Moldova - Republic of',
            'Monaco',
            'Mongolia',
            'Montenegro',
            'Montserrat',
            'Morocco',
            'Mozambique',
            'Myanmar',
            'Namibia',
            'Nauru',
            'Nepal',
            'Netherlands',
            'New Caledonia',
            'New Zealand',
            'Nicaragua',
            'Niger',
            'Nigeria',
            'Niue',
            'Norfolk Island',
            'Northern Mariana Islands',
            'Norway',
            'Oman',
            'Pakistan',
            'Palau',
            'Palestinian Territory - Occupied',
            'Panama',
            'Papua New Guinea',
            'Paraguay',
            'Peru',
            'Philippines',
            'Pitcairn',
            'Poland',
            'Portugal',
            'Puerto Rico',
            'Qatar',
            'Réunion',
            'Romania',
            'Russian Federation',
            'Rwanda',
            'Saint Barthélemy',
            'Saint Helena - Ascension and Tristan da Cunha',
            'Saint Kitts and Nevis',
            'Saint Lucia',
            'Saint Martin (French part)',
            'Saint Pierre and Miquelon',
            'Saint Vincent and the Grenadines',
            'Samoa',
            'San Marino',
            'Sao Tome and Principe',
            'Saudi Arabia',
            'Senegal',
            'Serbia',
            'Seychelles',
            'Sierra Leone',
            'Singapore',
            'Sint Maarten (Dutch part)',
            'Slovakia',
            'Slovenia',
            'Solomon Islands',
            'Somalia',
            'South Africa',
            'South Georgia and the South Sandwich Islands',
            'South Sudan',
            'Spain',
            'Sri Lanka',
            'Sudan',
            'Suriname',
            'Svalbard and Jan Mayen',
            'Swaziland',
            'Sweden',
            'Switzerland',
            'Syrian Arab Republic',
            'Taiwan - Province of China',
            'Tajikistan',
            'Tanzania - United Republic of',
            'Thailand',
            'Timor-Leste',
            'Togo',
            'Tokelau',
            'Tonga',
            'Trinidad and Tobago',
            'Tunisia',
            'Turkey',
            'Turkmenistan',
            'Turks and Caicos Islands',
            'Tuvalu',
            'Uganda',
            'Ukraine',
            'United Arab Emirates',
            'United Kingdom',
            'United States',
            'United States Minor Outlying Islands',
            'Uruguay',
            'Uzbekistan',
            'Vanuatu',
            'Venezuela - Bolivarian Republic of',
            'Viet Nam',
            'Virgin Islands - British',
            'Virgin Islands - U.S.',
            'Wallis and Futuna',
            'Western Sahara',
            'Yemen',
            'Zambia',
            'Zimbabwe'
        );
        $short = array(
            'AF',
            'AX',
            'AL',
            'DZ',
            'AS',
            'AD',
            'AO',
            'AI',
            'AQ',
            'AG',
            'AR',
            'AM',
            'AW',
            'AU',
            'AT',
            'AZ',
            'BS',
            'BH',
            'BD',
            'BB',
            'BY',
            'BE',
            'BZ',
            'BJ',
            'BM',
            'BT',
            'BO',
            'BQ',
            'BA',
            'BW',
            'BV',
            'BR',
            'IO',
            'BN',
            'BG',
            'BF',
            'BI',
            'KH',
            'CM',
            'CA',
            'CV',
            'KY',
            'CF',
            'TD',
            'CL',
            'CN',
            'CX',
            'CC',
            'CO',
            'KM',
            'CG',
            'CD',
            'CK',
            'CR',
            'CI',
            'HR',
            'CU',
            'CW',
            'CY',
            'CZ',
            'DK',
            'DJ',
            'DM',
            'DO',
            'EC',
            'EG',
            'SV',
            'GQ',
            'ER',
            'EE',
            'ET',
            'FK',
            'FO',
            'FJ',
            'FI',
            'FR',
            'GF',
            'PF',
            'TF',
            'GA',
            'GM',
            'GE',
            'DE',
            'GH',
            'GI',
            'GR',
            'GL',
            'GD',
            'GP',
            'GU',
            'GT',
            'GG',
            'GN',
            'GW',
            'GY',
            'HT',
            'HM',
            'VA',
            'HN',
            'HK',
            'HU',
            'IS',
            'IN',
            'ID',
            'IR',
            'IQ',
            'IE',
            'IM',
            'IL',
            'IT',
            'JM',
            'JP',
            'JE',
            'JO',
            'KZ',
            'KE',
            'KI',
            'KP',
            'KR',
            'KW',
            'KG',
            'LA',
            'LV',
            'LB',
            'LS',
            'LR',
            'LY',
            'LI',
            'LT',
            'LU',
            'MO',
            'MK',
            'MG',
            'MW',
            'MY',
            'MV',
            'ML',
            'MT',
            'MH',
            'MQ',
            'MR',
            'MU',
            'YT',
            'MX',
            'FM',
            'MD',
            'MC',
            'MN',
            'ME',
            'MS',
            'MA',
            'MZ',
            'MM',
            'NA',
            'NR',
            'NP',
            'NL',
            'NC',
            'NZ',
            'NI',
            'NE',
            'NG',
            'NU',
            'NF',
            'MP',
            'NO',
            'OM',
            'PK',
            'PW',
            'PS',
            'PA',
            'PG',
            'PY',
            'PE',
            'PH',
            'PN',
            'PL',
            'PT',
            'PR',
            'QA',
            'RE',
            'RO',
            'RU',
            'RW',
            'BL',
            'SH',
            'KN',
            'LC',
            'MF',
            'PM',
            'VC',
            'WS',
            'SM',
            'ST',
            'SA',
            'SN',
            'RS',
            'SC',
            'SL',
            'SG',
            'SX',
            'SK',
            'SI',
            'SB',
            'SO',
            'ZA',
            'GS',
            'SS',
            'ES',
            'LK',
            'SD',
            'SR',
            'SJ',
            'SZ',
            'SE',
            'CH',
            'SY',
            'TW',
            'TJ',
            'TZ',
            'TH',
            'TL',
            'TG',
            'TK',
            'TO',
            'TT',
            'TN',
            'TR',
            'TM',
            'TC',
            'TV',
            'UG',
            'UA',
            'AE',
            'GB',
            'US',
            'UM',
            'UY',
            'UZ',
            'VU',
            'VE',
            'VN',
            'VG',
            'VI',
            'WF',
            'EH',
            'YE',
            'ZM',
            'ZW'
        );
        //$in = strtolower(trim($in));
        switch ($type)
        {
            case 'long':
                $out = str_replace($short, $long, $in);
                break;
            case 'short':
                $out = str_replace($long, $short, $in);
                break;
        } //$type
        return $out;
    }
    /**
     * @desc Test the different !!TESTABLE!! functions
     * @param none
     * @return none
     * @author Glenn Van Schil
     */
    function test()
    {
        //Test fillMap()
        $test           = $this->fillMap();
        $expectedResult = 'is_array';
        $testName       = "Test fillMap()";
        $notes          = "Checks if fillMap returns an array of data";

        $this->unit->run($test, $expectedResult, $testName, $notes);

        //Test fillGeo()
        $test           = $this->fillGeo();
        $expectedResult = 'is_array';
        $testName       = "Test fillGeo()";
        $notes          = "Checks if fillGeo() returns an array of data";

        $this->unit->run($test, $expectedResult, $testName, $notes);

        //Test donutTypes()
        $test           = $this->donutTypes();
        $expectedResult = 'is_array';
        $testName       = "Test donutTypes()";
        $notes          = "Checks if donutTypes() returns an array of data";

        $this->unit->run($test, $expectedResult, $testName, $notes);

        //Test donutAttacks()
        $test           = $this->donutAttacks();
        $expectedResult = 'is_array';
        $testName       = "Test donutAttacks()";
        $notes          = "Checks if donutAttacks() returns an array of data";

        $this->unit->run($test, $expectedResult, $testName, $notes);

        //Test myCodes()
        //1
        $test           = $this->myCodes('BE', 'long');
        $expectedResult = 'Belgium';
        $testName       = "Test myCodes('BE','long')";
        $notes          = "The parameters are 2 strings country and type, type can either be 'long' or 'short'";

        $this->unit->run($test, $expectedResult, $testName, $notes);

        //2
        $test           = $this->myCodes('Belgium', 'short');
        $expectedResult = 'BE';
        $testName       = "Test myCodes('belgium','short')";
        $notes          = "The parameters are 2 strings country and type, type can either be 'long' or 'short'";

        $this->unit->run($test, $expectedResult, $testName, $notes);


        $uri = 'test/controllerTests/test_location.html';
        write_file($uri, '<h1>Test Results Location</h1>');
        write_file($uri, $this->unit->report(), 'a');
    }
}
?>
