<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc Redirects the user to the portal on a successfull login or back to the login screen when failed
 * @author Glenn Van Schil
 */
class Main extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * @desc Show the login panel
     * @param none
     * @return loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
        $data = $this->getData();
        $data['honeylist'] = $this->honeyList();
        if ($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $this->load->view('view_header', $data);
            $this->load->view('view_home', $data);
            $this->load->view('view_footer');
        } //$this->session->userdata('logged_in')
        else
        {
            //If no session, redirect to login page
            $this->load->helper(array(
                'form'
            ));
            $this->load->view('view_main', $data);
        }
    }
}
?>
