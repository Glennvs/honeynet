<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc The functions within this class control the different VM's
 * @author Glenn Van Schil
 * @author Lennart Pollaris
 */
session_start();
class Portal extends MY_Controller
{
    public $connection;
    function __construct()
    {
        parent::__construct();
        $this->config->load('esxi');
        $ip = $this->config->item('ip');
        $port = $this->config->item('port');
        $this->connection = ssh2_connect($ip, $port);
        $user = $this->config->item('user');
        $password = $this->config->item('password');
        ssh2_auth_password($this->connection, $user, $password);
    }

    /**
     * @desc opens the portal
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
    	$this->test();
        $data           = $this->getData();
        $data['vmlist'] = $this->setTableData();
        $data['power']  = $this->getVmState($data['vmlist']);
        $data['up']     = $this->getUpTime($data['vmlist']);
        $data['honeylist'] = $this->honeyList();
        $data['id'] = $this->jsVar();

        if ($this->session->userdata('logged_in'))
        {
            $sess_array = $this->session->userdata('logged_in');
            if ($sess_array['admin'] != 0)
            {
                $session_data = $this->session->userdata('logged_in');
                $this->load->view('view_header', $data);
                $this->load->view('view_portal', $data);
                $this->load->view('view_footer');
            } //$sess_array['admin'] != 0
            else
            {
                $session_data = $this->session->userdata('logged_in');
                $this->load->view('view_header', $data);
                $this->load->view('view_error', $data);
                $this->load->view('view_footer');
            }
        } //$this->session->userdata('logged_in')
        else
        {
            redirect('main', 'refresh');
        }
    }

    /**
     * @desc Send data from url to view for notifications
     * @param /
     * @return /
     * @author Lennart Pollaris
     */
    function jsVar()
    {
    	$list = $this->setTableData();

    	if($this->uri->segment(3) == TRUE){
    	$name = array_search($this->uri->segment(3), $list);
    	return array('ID' => $this->uri->segment(3),'Name' => $list[$name+1],'Button' => $this->uri->segment(2));}
    }

    /**
     * @desc Get data of vms from esxi and set in array
     * @param none
     * @return array with vms data ( id and name )
     * @author Glenn Van Schil
     */
    function setTableData()
    {

        $output = $this->sendCommandEsxi('vim-cmd vmsvc/getallvms | sed \'1d\' | awk \'{if ($1 > 0) print $1":"$2}\' | grep \'^[0-9]\{1,\}:[a-zA-Z]\{1,\}\'');
        $vmlist = str_word_count($output, 1, '1234567890/._-#$%*');
        return $vmlist;
    }

    /**
     * @desc Get state of vm from esxi
     * @param $ids - array with ids of vms on esxi
     * @return array with the state of all vms ON or OFF
     * @author Lennart Pollaris
     */
    function getVmState($ids)
    {

        $counter = 0;
        for ($i = 0; $i < count($ids); $i = $i + 2)
        {
            $command         = 'vim-cmd vmsvc/power.getstate ' . $ids[$i];
            $split           = explode(" ", $this->sendCommandEsxi($command));
            $power[$counter] = $split[count($split) - 1];
            $counter++;

        }


        return $power;
    }

    /**
     * @desc Send on command to esxi
     * @param $id from vm
     * @return /
     * @author Lennart Pollaris
     */
    function turnOn($id)
    {
        $command = 'vim-cmd vmsvc/power.on ' . $id;
        $output  = $this->sendCommandEsxi($command);
        $this->index();
    }

    /**
     * @desc Send off command to esxi
     * @param $id from vm
     * @return /
     * @author Lennart Pollaris
     */
    function turnOff($id)
    {
        $command = 'vim-cmd vmsvc/power.off ' . $id;
        $output  = $this->sendCommandEsxi($command);
        $this->index();
    }

	/**
	 * @desc Create new server on esxi
	 * @param /
	 * @return /
	 * @author Lennart Pollaris
	 */
    function newServer()
    {
    	$honeypot = $this->input->post('honeypot');
    	if($honeypot == 'kippo')
    	{
    		$servername = $this->input->post('name');
    		$command1   = 'cd /vmfs/volumes/datastore1 ; mkdir "' . $servername . '" ;ls;';
    		$command2   = 'cd /vmfs/volumes/datastore1/' . $servername . ' ; cp /vmfs/volumes/datastore1/newkippo/newkippo.vmx "' . $servername . '.vmx"';
    		$command3   = 'cd /vmfs/volumes/datastore1/' . $servername . ' ; sed -i s/newkippo/' . $servername . '/g ' . $servername . '.vmx';
    		$command4   = 'vmkfstools -d thin -i /vmfs/volumes/datastore1/newkippo/newkippo.vmdk /vmfs/volumes/datastore1/' . $servername . '/' . $servername . '.vmdk';
    		$command5   = 'vim-cmd solo/registervm /vmfs/volumes/datastore1/' . $servername . '/' . $servername . '.vmx';
    	}
    	else
    	{
    		$servername = $this->input->post('name');
    		$command1   = 'cd /vmfs/volumes/datastore1 ; mkdir "' . $servername . '" ;ls;';
    		$command2   = 'cd /vmfs/volumes/datastore1/' . $servername . ' ; cp /vmfs/volumes/datastore1/newlabrea/newlabrea.vmx "' . $servername . '.vmx"';
    		$command3   = 'cd /vmfs/volumes/datastore1/' . $servername . ' ; sed -i s/newlabrea/' . $servername . '/g ' . $servername . '.vmx';
    		$command4   = 'vmkfstools -d thin -i /vmfs/volumes/datastore1/newlabrea/newlabrea.vmdk /vmfs/volumes/datastore1/' . $servername . '/' . $servername . '.vmdk';
    		$command5   = 'vim-cmd solo/registervm /vmfs/volumes/datastore1/' . $servername . '/' . $servername . '.vmx';
    	}




        $this->sendCommandEsxi($command1);
        $this->sendCommandEsxi($command2);
        $this->sendCommandEsxi($command3);
        $this->sendCommandEsxi($command4);
        $this->sendCommandEsxi($command5);

        //answer on question "I copied it"
        $list = $this->setTableData();
        $namepos = array_search($servername, $list);
        $id = $list[$namepos-1];
        $command6 = 'vim-cmd vmsvc/power.on '.$id.'& sleep 3 ; kill $!';
        $this->sendCommandEsxi($command6);
        $command7 = 'vim-cmd vmsvc/message '.$id.' | grep -o "_[a-zA-Z]\{1,\}[0-9]\{1,\}"';
        $vmx = $this->sendCommandEsxi($command7);
        $command8 = 'vim-cmd vmsvc/message ' . $id .' ' . $vmx .' 2';
        $this->sendCommandEsxi($command8);

        $this->index();

    }

	/**
	 * @desc Delete vm on esxi
	 * @param $id and name from vm
	 * @return /
	 * @author Lennart Pollaris
	 */
    function deleteVm($id, $name)
    {
        $command1 = 'vim-cmd vmsvc/power.off ' . $id;
        $command2 = 'vim-cmd vmsvc/unregister ' . $id;
        $command3 = 'rm -R /vmfs/volumes/datastore1/' . $name;
        $this->sendCommandEsxi($command1);
        $this->sendCommandEsxi($command2);
        $this->sendCommandEsxi($command3);
        $this->index();
    }

	/**
	 * @desc Send uptime command to esxi
	 * @param $id from vm
	 * @return uptime from vm hh:mm:ss
	 * @author Lennart Pollaris
	 */
    function getUpTime($id)
    {
        $counter = 0;
        for ($i = 0; $i < count($id); $i = $i + 2)
        {
            $command = 'vim-cmd vmsvc/get.summary ' . $id[$i] . ' | grep uptimeSeconds';


            $up[$counter] = gmdate("H:i:s", intval(preg_replace('/[^0-9]+/', '', $this->sendCommandEsxi($command)), 10));
            $counter++;
		}
		return $up;
	}

	/**
	 * @desc Send reboot command to esxi
	 * @param $id from vm
	 * @return /
	 * @author Lennart Pollaris
	 */
	function reboot($id)
	{
		$command = 'vim-cmd vmsvc/power.reset '. $id;
		$this->sendCommandEsxi($command);
		sleep(4);
		$this->index();
	}

    /**
     * @desc Send commands to esxi via ssh
     * @param $command - command that will send to esxi
     * @return ssh output from esxi
     * @author Lennart Pollaris
     */
    function sendCommandEsxi($command)
    {
        $stream = ssh2_exec($this->connection, $command);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);
        return $output;
    }

    function test()
    {
 			$testData = array(1,2);

            $test           = $this->setTableData();
            $expectedResult = 'is_array';
            $testName       = "Test setTableData()";
            $notes          = "Checks if setTableData() returns an array of data, the data in the array are the id's and the names of the servers";

         	$this->unit->run($test, $expectedResult, $testName, $notes) ;


    		$test           = $this->sendCommandEsxi('vim-cmd vmsvc/power.getstate 1');
            $expectedResult = 'is_string';
            $testName       = "Test sendCommandEsxi('vim-cmd vmsvc/power.getstate 1')";
            $notes          = "Checks if the function returns a string, the string is the output of a ssh command send to the esxi server. The input is a ssh command that will be send to the esxi server";

         	$this->unit->run($test, $expectedResult, $testName, $notes) ;


    		$test           = $this->getVmState($testData);
            $expectedResult = 'is_array';
            $testName       = "Test getVmState()";
            $notes          = "Checks if the function returns an array, in the array are the values ON or OFF for all the virtual machines. The input is an array will the id's of all the virtual machines";

    		$this->unit->run($test, $expectedResult, $testName, $notes) ;

    		$test           = $this->getUpTime($testData);
            $expectedResult = 'is_array';
            $testName       = "Test getUpTime()";
            $notes          = "Checks if the function returns an array, in the array is the uptime of all the virtual machines. The input is an array will the id's of all the virtual machines";

    		$this->unit->run($test, $expectedResult, $testName, $notes) ;



            $uri = './home/server/Desktop/test.txt';
            write_file($uri, '<h1>Test Results Portal</h1>');
            write_file($uri, $this->unit->report(), 'a');
	}
}
?>
