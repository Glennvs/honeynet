<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @desc Contain different functions for managing the users own account
 * @author Glenn Van Schil
 */
class Settings extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    /**
     * @desc Validates the username
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function username()
    {
        $data = $this->getData();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required|trim|required|max_length[20]|xss_clean|is_unique[users.username]|alpha_numeric');

        if ($this->form_validation->run() != FALSE)
        {
            $this->updateUser();
            redirect('homepage/settings');
        } //$this->form_validation->run() == True
        else
        {
            $this->load->view('view_header', $data);
            $this->load->view('view_settings', $data);
            $this->load->view('view_footer');
        }
    }

    /**
     * @desc Validates the password
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function password()
    {
        $data = $this->getData();
        $data['honeylist'] = $this->honeyList();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|trim|required|xss_clean|max_length[100]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|required|xss_clean|max_length[100]');

        if ($this->form_validation->run() != FALSE)
        {
            $this->updatePass();
            redirect('homepage/settings');
        } //$this->form_validation->run() == True
        else
        {
            $this->load->view('view_header', $data);
            $this->load->view('view_settings', $data);
            $this->load->view('view_footer');
        }
    }

    /**
     * @desc Updates the User's username
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function updateUser()
    {
        if ($this->input->post('updateUser')) //$_POST["update"];
        {
            if ($this->model_user->updateUser())
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }

    /**
     * @desc Updates the User's password
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function updatePass()
    {
        if ($this->input->post('updatePass')) //$_POST["update"];
        {
            if ($this->model_user->updatePass())
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }
}
?>