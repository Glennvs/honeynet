<?php
/**
 * @desc Contain different functions for deleting/adding/updating users and admins
 * @author Glenn Van Schil
 */
class User extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc Validates the submited form
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
        $this->globalTest();
        $this->test();

        $data          = $this->getData();
        $data['honeylist'] = $this->honeyList();
        $data['users'] = $this->getUsername();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required|trim|required|xss_clean|max_length[20]|is_unique[users.username]|alpha_numeric');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|trim|required|xss_clean|max_length[100]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');

        if ($this->form_validation->run() != FALSE)
        {
            $this->doRegister();
            redirect('/admin');
        } //$this->form_validation->run() == True
        else
        {
            $this->load->view('view_header', $data);
            $this->load->view('view_admin', $data);
            $this->load->view('view_footer');
        }
    }

    /**
     * @desc deletes a user from the database
     * @param $username - selected by admin in the admin panel
     * @return reloads the admin panel
     * @author Glenn Van Schil
     */
    function delete($username)
    {
        $this->model_user->deleteUsers($username);
        redirect('/admin');
    }

    /**
     * @desc adds a user to the database
     * @param none
     * @return reloads the admin panel
     * @author Glenn Van Schil
     */
    function doRegister()
    {
        if ($this->input->post('register'))
        {
            if ($this->model_user->addUser())
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }

    /**
     * @desc makes existing user an admin
     * @param none
     * @return reloads the admin panel
     * @author Glenn Van Schil
     */
    public function addAdmin($username)
    {
        {
            if ($this->model_user->updateAdmin($username))
            {
                redirect('/admin');
            }
            else
            {
                redirect('/admin');
            }
        }
    }

    /**
     * @desc Loads all the users in the database
     * @param none
     * @return array - contains the strings
     * @author Glenn Van Schil
     */
    function getUsername()
    {
        $result = $this->model_user->showUsers();

        $users = array();
        if (!empty($result))
        {
            foreach ($result as $row)
            {
                array_push($users, array(
                    $row->username,
                    $row->admin
                ));
            } //$result as $row

            return $users;
        } //!empty($result)
    }

    function test()
    {
        $testData = array();

        //Test getUsername()
        $test           = $this->getUsername();
        $expectedResult = 'is_array';
        $testName       = "Test getUsername";
        $notes          = "Checks if getUsername() returns an array of data";

        $result = $this->unit->run($test, $expectedResult, $testName, $notes);

        array_push($testData, $result);

        $uri = 'test/controllerTests/test_user.html';
        write_file($uri, '<h1>Test Results user</h1>');
        write_file($uri, $this->unit->report(), 'a');
    }
}
?>