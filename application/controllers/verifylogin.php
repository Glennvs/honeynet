
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc When a user tries to log-in it will check if the user exists or not
 * @author Glenn Van Schil
 */
class VerifyLogin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @desc validates the form
     * @param none
     * @return Loads the requested page
     * @author Glenn Van Schil
     */
    function index()
    {
        $data = $this->getData();
        $data['honeylist'] = $this->honeyList();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required|trim|required|xss_clean|max_length[20]|alpha_numeric');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|required|xss_clean|max_length[100]|callback_checkDatabase');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('view_main', $data);
        } //$this->form_validation->run() == FALSE
        else
        {
            redirect('homepage', 'refresh');
        }

    }

    /**
     * @desc checks if the user has a valid username and password
     * @param string $password - the password entered by the user
     * @return boolean - valid or invalid user
     * @author Glenn Van Schil
     */
    function checkDatabase($password)
    {
        $username = $this->input->post('username');

        $result = $this->model_user->login($username, $password);

        if ($result)
        {
            $sess_array = array();
            foreach ($result as $row)
            {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username,
                    'admin' => $row->admin
                );
                $this->session->set_userdata('logged_in', $sess_array);
            } //$result as $row
            return TRUE;
        } //$result
        else
        {
            $this->form_validation->set_message('checkDatabase', 'Invalid username or password');
            return false;
        }
    }
}
?>
