<?php

/**
 * @desc Custom base class for functions needed in almost every controller
 * @author Glenn Van Schil
 * @author Lennart Pollaris
 */
class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_location', '', TRUE);
        $this->load->model('model_user', '', TRUE);
        $this->load->model('model_history', '', TRUE);
        $this->load->model('model_download', '', TRUE);
        $this->load->model('model_command', '', TRUE);
        $this->load->model('model_attack', '', TRUE);
        $this->load->model('model_honeypot', '', TRUE);
        $this->lang->load('message');
    }

    /**
     * @desc loads the existing honeypots, if a honeypot exists it will add the right links and refs to the matching html page
     * @param none
     * @return String of html code
     * @author Glenn Van Schil
     */
    function honeyList()
    {
        $result = $this->model_honeypot->getHoneypots();
        $honeylist = "";
        foreach ($result as $row)
        {
            if($row->type == "kippo")
            {
                $honeylist = $honeylist."<li class=\"dropdown dropdown-submenu\">
                                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">".$row->type."</a>
                                            <ul class=\"dropdown-menu\">
                                                <li id=\"droplist\"><a href=". site_url(). "/attack>" . $this->lang->line('header_attacks') . "</a></li>
                                                <li id=\"droplist\"><a href=".site_url(). "/command/getCommands>" . $this->lang->line('header_commands') . "</a></li>
                                                <li id=\"droplist\"><a href=". site_url()."/download>" . $this->lang->line('header_downloads') . "</a></li>
                                            </ul>
                                        </li>";
            }
            else if($row->type == "labrea")
            {
                $honeylist = $honeylist."<li class=\"dropdown dropdown-submenu\">
                                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">".$row->type."</a>
                                            <ul class=\"dropdown-menu\">
                                                <li id=\"droplist\"><a href=". site_url(). "/attack/labrea>" . $this->lang->line('header_attacks') . "</a></li>
                                            </ul>
                                        </li>";
            }
            else if($row->type == "dionaea")
            {
                $honeylist = $honeylist."<li class=\"dropdown dropdown-submenu\">
                                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">".$row->type."</a>
                                            <ul class=\"dropdown-menu\">
                                                <li id=\"droplist\"><a href=". site_url(). "/attack>" . $this->lang->line('header_attacks') . "</a></li>
                                            </ul>
                                        </li>";
            }
        }

        return $honeylist;
    }

        /**
     * @desc Loads the page text
     * @param none
     * @return array - contains the strings
     * @author Glenn Van Schil
     */
    function getData()
    {
        //Header
        $data['header_honeynet']  = $this->lang->line('header_honeynet');
        $data['header_logout']    = $this->lang->line('header_logout');
        $data['header_map']       = $this->lang->line('header_map');
        $data['header_admin']     = $this->lang->line('header_admin');
        $data['header_attacks']   = $this->lang->line('header_attacks');
        $data['header_settings']  = $this->lang->line('header_settings');
        $data['header_history']   = $this->lang->line('header_history');
        $data['header_portal']    = $this->lang->line('header_portal');
        $data['header_commands']  = $this->lang->line('header_commands');
        $data['header_honeypots'] = $this->lang->line('header_honeypots');
        $data['header_downloads'] = $this->lang->line('header_downloads');
        //Admin
        $data['admin_titel']      = $this->lang->line('admin_titel');
        $data['admin_delete']     = $this->lang->line('admin_delete');
        $data['admin_username']   = $this->lang->line('admin_username');
        $data['admin_insert']     = $this->lang->line('admin_insert');
        $data['admin_make']       = $this->lang->line('admin_make');
        //No Admin
        $data['no_admin_titel']   = $this->lang->line('no_admin_titel');
        $data['no_admin_intro']   = $this->lang->line('no_admin_intro');
        //Login
        $data['login_titel']    = $this->lang->line('login_titel');
        $data['login_username'] = $this->lang->line('login_username');
        $data['login_password'] = $this->lang->line('login_password');
        $data['login_button']   = $this->lang->line('login_button');
        //Settings
        $data['settings_titel']   = $this->lang->line('settings_titel');
        $data['settings_update']  = $this->lang->line('settings_update');
        //Portal
        $data['portal_titel']     = $this->lang->line('portal_titel');
        $data['portal_vm_id']     = $this->lang->line('portal_vm_id');
        $data['portal_vm_name']   = $this->lang->line('portal_vm_name');
        $data['portal_vm_power']  = $this->lang->line('portal_vm_power');
        $data['portal_add_vm']    = $this->lang->line('portal_add_vm');
        $data['portal_vm_delete']    = $this->lang->line('portal_vm_delete');
        $data['portal_vm_reboot']    = $this->lang->line('portal_vm_reboot');
        $data['portal_vm_uptime']    = $this->lang->line('portal_vm_uptime');
        //Home
        $data['home_titel']       = $this->lang->line('home_titel');
        $data['home_intro']       = $this->lang->line('home_intro');
        //Location
        $data['map_titel']        = $this->lang->line('map_titel');
        //History
        $data['history_titel']     = $this->lang->line('history_titel');
        $data['history_lastWeek']  = $this->lang->line('history_lastWeek');
        $data['history_lastTwo']   = $this->lang->line('history_lastTwo');
        $data['history_lastThree'] = $this->lang->line('history_lastThree');
        $data['history_lastMonth'] = $this->lang->line('history_lastMonth');
        //downloads
        $data['downloads_titel']     = $this->lang->line('downloads_titel');
        $data['downloads_session']   = $this->lang->line('downloads_session');
        $data['downloads_timestamp'] = $this->lang->line('downloads_timestamp');
        $data['downloads_url']       = $this->lang->line('downloads_url');
        $data['downloads_download']  = $this->lang->line('downloads_download');
        //Commands
        $data['commands_titel']     = $this->lang->line('commands_titel');
        $data['commands_session']   = $this->lang->line('commands_session');
        $data['commands_timestamp'] = $this->lang->line('commands_timestamp');
        $data['commands_realm']     = $this->lang->line('commands_realm');
        $data['commands_success']   = $this->lang->line('commands_success');
        $data['commands_input']     = $this->lang->line('commands_input');
        $data['commands_date']      = $this->lang->line('commands_date');
        //Attacks Kippo
        $data['attacks_titel']    = $this->lang->line('attacks_titel');
        $data['attacks_session']  = $this->lang->line('attacks_session');
        $data['attacks_success']  = $this->lang->line('attacks_success');
        $data['attacks_username'] = $this->lang->line('attacks_username');
        $data['attacks_password'] = $this->lang->line('attacks_password');
        $data['attacks_time']     = $this->lang->line('attacks_time');

        //Attacks Labrea
        $data['attacksLabrea_receiver'] = $this->lang->line('attacksLabrea_receiver');
        $data['attacksLabrea_sender'] = $this->lang->line('attacksLabrea_sender');
        $data['attacksLabrea_message'] = $this->lang->line('attacksLabrea_message');
        $data['attacksLabrea_date'] = $this->lang->line('attacksLabrea_date');
        $data['attacksLabrea_titel'] = $this->lang->line('attacksLabrea_titel');

        return $data;
    }

    function globalTest()
        {
            $testData = array();

            //Test honeyList()
            $test           = $this->honeyList();
            $expectedResult = 'is_string';
            $testName       = "Test honeyList()";
            $notes          = "Checks if the server contains a certain type of honeypot";

            $result = $this->unit->run($test, $expectedResult, $testName, $notes);

            array_push($testData, $result);

            //Test getData()
            $test           = $this->getData();
            $expectedResult = 'is_array';
            $testName       = "Test getData()";
            $notes          = "Checks if the function is returning all the text data";

            $result = $this->unit->run($test, $expectedResult, $testName, $notes);

            array_push($testData, $result);

            $uri = 'test/controllerTests/test_global.html';
            write_file($uri, '<h1>Test of global functions</h1>');
            write_file($uri, $this->unit->report(), 'a');
        }
}

?>