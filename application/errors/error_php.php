<style type="text/css" media="screen">
	.error{
	    color: red;
	}
</style>
<div class="container">
        <div class="well error">
            <h4>A PHP Error was encountered</h4>

            <p>Severity: <?php echo $severity; ?></p>
            <p>Message:  <?php echo $message; ?></p>
            <p>Filename: <?php echo $filepath; ?></p>
            <p>Line Number: <?php echo $line; ?></p>
        </div>
</div>