<?php
//Home
$lang["home_titel"] = "Honeynet";
$lang["home_intro"] = "Welcome to the Cegeka Honeynet portal where you can manage and retrieve the collected data";

//Portal
$lang["portal_titel"]    = "Portal";
$lang["portal_vm_id"]    = "ID";
$lang["portal_vm_name"]  = "Name";
$lang["portal_vm_onoff"] = "ON/OFF";
$lang["portal_vm_power"] = "Change Power";
$lang["portal_add_vm"]   = "Add server";
$lang["portal_vm_reboot"]   = "Reboot";
$lang["portal_vm_delete"]   = "Delete";
$lang["portal_vm_uptime"]   = "Uptime";

//Map
$lang["map_titel"] = "Visual Map";

//Attacks Kippo
$lang["attacks_titel"]    = "Attacks Kippo";
$lang["attacks_session"]  = "Session";
$lang["attacks_success"]  = "Success";
$lang["attacks_username"] = "Username";
$lang["attacks_password"] = "Password";
$lang["attacks_time"]     = "Timestamp";

//Attacks Labrea
$lang['attacksLabrea_receiver'] = "Receiver";
$lang['attacksLabrea_sender'] = "Sender";
$lang['attacksLabrea_message'] = "Message";
$lang['attacksLabrea_date'] = "Date";
$lang['attacksLabrea_titel'] = "Attacks Labrea";

//Admin
$lang["admin_titel"]    = "Admin panel";
$lang["admin_delete"]   = "Delete";
$lang["admin_username"] = "Username";
$lang["admin_insert"]   = "Insert";
$lang["admin_make"]     = "Make admin";

//No Admin
$lang["no_admin_titel"] = "Sorry";
$lang["no_admin_intro"] = "The action you are trying to perform is not possible";

//Login
$lang["login_titel"]    = "Please sign in";
$lang["login_username"] = "Username";
$lang["login_password"] = "Password";
$lang["login_button"]   = "Login";

//Settings
$lang["settings_titel"]  = "Settings";
$lang["settings_update"] = "Update";

//Header
$lang["header_honeynet"]  = "Honeynet";
$lang["header_logout"]    = "Logout";
$lang["header_map"]       = "Map";
$lang["header_admin"]     = "Admin";
$lang["header_attacks"]   = "Attacks";
$lang["header_settings"]  = "Settings";
$lang["header_history"]   = "History";
$lang["header_portal"]    = "Portal";
$lang["header_commands"]  = "Commands";
$lang["header_honeypots"] = "Honeypots";
$lang["header_downloads"] = "Downloads";

//History
$lang["history_titel"]     = "History";
$lang["history_lastWeek"]  = "Last week";
$lang["history_lastTwo"]   = "Last two weeks";
$lang["history_lastThree"] = "Last three weeks";
$lang["history_lastMonth"] = "Last month";

//Commands
$lang["commands_titel"]     = "Commands";
$lang["commands_session"]   = "Session";
$lang["commands_timestamp"] = "Timestamp";
$lang["commands_realm"]     = "Realm";
$lang["commands_succes"]    = "Succes";
$lang["commands_input"]     = "Input";
$lang["commands_date"]      = "Date";



//Downloads
$lang["downloads_titel"]     = "Downloads";
$lang["downloads_session"]   = "Session";
$lang["downloads_timestamp"] = "Timestamp";
$lang["downloads_url"]       = "URL";
$lang["downloads_download"]  = "Download";
?>
