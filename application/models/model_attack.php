<?php
Class Model_Attack extends CI_Model
{
    /**
      * @desc Executes a query to show all attack data
      * @param none
      * @return query result
      * @author Glenn Van Schil
    */
    function loadAttacks()
    {
        $this->db->select();
        $this->db->from('auth');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

    /**
      * @desc Executes a query to show all attack data
      * @param none
      * @return query result
      * @author Glenn Van Schil
    */
    function loadAttacksLabrea()
    {
        $this->db->select();
        $this->db->from('viewlabrea');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

}
?>