<?php
Class Model_Command extends CI_Model
{
    /**
     * @desc Executes a query to show all attack data
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function loadCommands()
    {
        $this->db->select();
        $this->db->from('viewcommands');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

    function loadDetails($session)
    {
        $query = $this->db->query("CALL proceduredetails(\"" . $session . "\");");

        $res = $query->result_array();

        $query->next_result();
        $query->free_result();

        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

}
?>