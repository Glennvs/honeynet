<?php
Class Model_Download extends CI_Model
{
    /**
     * @desc Executes a query to show all attack data
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function loadDownloads()
    {
        $this->db->select();
        $this->db->from('viewdownloads');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

}
?>