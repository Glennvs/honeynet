<?php
Class Model_History extends CI_Model
{
    /**
      * @desc Executes a query to sort all attacks by date and type
      * @param none
      * @return query result
      * @author Glenn Van Schil
    */
    function lastMonth()
    {
        $this->db->select();
        $this->db->from('viewlastmonth');
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
        {
            return $query->result();
        } //$query->num_rows() >= 1
        else
        {
            return false;
        }
    }
    function lastThree()
    {
        $this->db->select();
        $this->db->from('viewlastthree');
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
        {
            return $query->result();
        } //$query->num_rows() >= 1
        else
        {
            return false;
        }
    }

    function lastTwo()
    {
        $this->db->select();
        $this->db->from('viewlasttwo');
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
        {
            return $query->result();
        } //$query->num_rows() >= 1
        else
        {
            return false;
        }
    }

    function lastWeek()
    {
        $this->db->select();
        $this->db->from('viewlastweek');
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
        {
            return $query->result();
        } //$query->num_rows() >= 1
        else
        {
            return false;
        }
    }

}
?>