<?php
Class Model_Honeypot extends CI_Model
{
    /**
     * @desc loads a list of existing honeypots
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function getHoneypots()
    {
        $this->db->select('type');
        $this->db->from('honeypots');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }
}
?>