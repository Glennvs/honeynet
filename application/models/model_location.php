<?php
Class Model_Location extends CI_Model
{
    /**
     * @desc Executes a query to load all IP addresses
     * @param none
     * @return query result
     * @author Glenn
     */
    function loadIps()
    {

        $this->db->select();
        $this->db->from('viewmapdata');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

    /**
     * @desc Executes a query to get the country, type and amout of attacks per type
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function groupTypes()
    {
        $this->db->select();
        $this->db->from('viewgrouptypes');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

    /**
     * @desc Executes a query to get the type and amout of attacks per type
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function groupAttacks()
    {
        $this->db->select();
        $this->db->from('viewgroupattacks');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

}
?>