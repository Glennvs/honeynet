<?php
class Model_User extends CI_Model
{
    /**
     * @desc Executes a query to log the user in
     * @param $username - entered by the user
     * @param $password - entered by the user
     * @return query result
     * @author Glenn Van Schil
     */
    function login($username, $password)
    {
        $this->db->select('id, username, password, admin');
        $this->db->from('users');
        $this->db->where('username', $this->db->escape_str($username));
        $this->db->where('password', hash('sha256', $this->db->escape_str($password)));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1)
        {
            return $query->result();
        } //$query->num_rows() == 1
        else
        {
            return false;
        }
    }

    /**
     * @desc Executes a query to show all the usernames
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function showUsers()
    {
        $this->db->select('username,admin');
        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }

    /**
     * @desc Executes a query to delete a certain user
     * @param $username - selected by the admin
     * @return query result
     * @author Glenn Van Schil
     */
    function deleteUsers($username)
    {
        $query = $this->db->where('username', $this->db->escape_str($username));
        $query = $this->db->where('admin <', '1');
        $query = $this->db->delete('users');
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /**
     * @desc Executes a query to add a user
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function addUser()
    {
        $data = array(
            'username' => $this->db->escape_str($this->input->post('username')),
            'password' => hash('sha256', $this->db->escape_str($this->input->post('password'))),
            'admin' => $this->input->post('admin')
        );
        $this->db->insert('users', $data);
        return true;
    }

    /**
     * @desc Executes a query to update Admin
     * @param $username
     * @return query result
     * @author Glenn Van Schil
     */
    function updateAdmin($username)
    {
        $data = array(
            'admin' => '1'
        );
        $this->db->where('username', $this->db->escape_str($username));
        $this->db->update('users', $data);
        return true;
    }

    /**
     * @desc Executes a query to update the current user
     * @param none
     * @return query result
     * @author Glenn Van Schil
     */
    function updateUser()
    {
        $sess_array = $this->session->userdata('logged_in');
        $data       = array(
            'username' => $this->db->escape_str($this->input->post('username'))
        );
        $this->db->where('username', $sess_array['username']);
        $this->db->update('users', $data);
        $sess_array['username'] = $this->input->post('username');
        $this->session->set_userdata('logged_in', $sess_array);
        return true;
    }

    function updatePass()
    {
        $sess_array = $this->session->userdata('logged_in');
        $data       = array(
            'password' => hash('sha256', $this->db->escape_str($this->input->post('password')))
        );
        $this->db->where('username', $sess_array['username']);
        $this->db->update('users', $data);
        return true;
    }

    /**
     * @desc Executes a query to make a certain user admin
     * @param $username - provided user to make admin
     * @return query result
     * @author Glenn Van Schil
     */
    function admin($username)
    {
        $query = $this->db->get_where('users', array(
            'username' => $username
        ));
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } //$query->num_rows() > 0
        else
        {
            return false;
        }
    }
}
?>
