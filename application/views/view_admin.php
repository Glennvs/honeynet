<section>
    <title><?php echo $admin_titel?></title>
    <div class="well">
        <h2><?php echo $admin_titel?></h2>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <caption><?php echo $admin_delete?></caption>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $admin_username?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($users))
                            {
                            	foreach($users as $row){ ?>
                        <tr>
                            <td><?php echo $row[0]; ?></td>
                            <?php if($row[1] == 0){?>
                            <td><a class="btn btn-default" href="<?php echo site_url(); ?>/user/delete/<?php echo $row[0]; ?>" role="button"><?php echo $admin_delete?></a></td>
                            <?php }
                                else{
                                ?>
                            <td></td>
                            <?php } ?>
                            <?php if($row[1] == 0){?>
                            <td><a class="btn btn-default" href="<?php echo site_url(); ?>/user/addAdmin/<?php echo $row[0]; ?>" role="button"><?php echo $admin_make?></a></td>
                            <?php }
                                else{
                                   ?>
                            <td></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <caption><?php echo $admin_insert?></caption>
                <?php echo validation_errors(); ?>
                <form action="<?=site_url('user') ?>" method="post" class="form-horizontal">
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="text" placeholder="Username" name="username" onblur="this.value=removeSpaces(this.value);" required/>
                    </div>
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="password" placeholder="Password" name="password" required/>
                    </div>
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="password" placeholder="Password confirmation" name="passconf" required/>
                    </div>
                    <div class="form-group">
                        <div class="checkbox col-sm-offset-1 col-sm-10">
                            <input type="hidden" name="admin" value="0" />
                            <input type="checkbox" name="admin" value="1" />Admin
                        </div>
                    </div>
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-default" value="Add user" name="register"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script language="javascript" type="text/javascript">
    /**
     * removes all the whitespaces
     * @param string
     * @return string without whitespaces
     * @author Glenn Van Schil
     */
    function removeSpaces(string)
    {
    										    r	eturn string.split(' ').join('');
    }
</script>