<section>
    <title><?php echo $attacks_titel?></title>
    <div class="well">
        <h2><?php echo $attacks_titel?></h2>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $attacks_session?></th>
                            <th><?php echo $attacks_success?></th>
                            <th><?php echo $attacks_username?></th>
                            <th><?php echo $attacks_password?></th>
                            <th><?php echo $attacks_time?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($attack as $row) { ?>
                        <tr>
                            <?php foreach ($row as $col) { ?>
                                <td><?php echo $col ?></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>