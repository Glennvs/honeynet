<section>
    <title><?php echo $attacksLabrea_titel?></title>
    <div class="well">
        <h2><?php echo $attacksLabrea_titel?></h2>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $attacksLabrea_receiver?></th>
                            <th><?php echo $attacksLabrea_sender?></th>
                            <th><?php echo $attacksLabrea_message?></th>
                            <th><?php echo $attacksLabrea_date?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($attack as $row) { ?>
                        <tr>
                            <?php foreach ($row as $col) { ?>
                                <td><?php echo $col ?></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>