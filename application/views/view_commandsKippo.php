<section>
    <title><?php echo $commands_titel?></title>
    <div class="well">
        <h2><?php echo $commands_titel?></h2>
        <div class="row">
            <div class="col-xs-4" style="height:600px;overflow:auto;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $commands_session?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($command as $row) { ?>
                        <tr>
                            <?php foreach ($row as $col) { ?>
                            <td><a role="button" href="<?php echo site_url(); ?>/command/getCommands/<?php echo $col ?>" class="btn btn-default"><?php echo $col ?></a></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-7">
                <div style="height:600px;width:auto;border:1px solid #ccc;overflow:auto;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><?php echo $commands_date?></th>
                                <th><?php echo $commands_titel?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($input as $row) { ?>
                            <tr>
                                <?php foreach ($row as $col) { ?>
                                <td>
                                    <?php echo $col ?>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>