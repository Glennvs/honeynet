<section>
    <title><?php echo $downloads_titel?></title>
    <div class="well">
        <h2><?php echo $downloads_titel?></h2>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $downloads_session?></th>
                            <th><?php echo $downloads_timestamp?></th>
                            <th><?php echo $downloads_url?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i=0; $i < count($download); $i++) { ?>
                        <tr>
                            <?php for ($j=0; $j < count($download[$i]); $j++) { ?>
                            <td><?php echo $download[$i][$j] ?></td>
                            <?php } ?>
                            <td max-width="100px">
                                <a role="button" href="<?php echo base_url(); ?>serverDownload/<?php echo $files[$i] ?>" class="btn btn-default"><?php echo $downloads_download?></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>