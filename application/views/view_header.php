<!doctype html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?php echo base_url("css/normalize.css"); ?>">
    <link href="<?php echo base_url("css/bootstrap.css"); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url("css/opmaak.css"); ?>">
    <link href="<?php echo base_url("css/toastr.css"); ?>" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php echo base_url("js/jquery.js"); ?>"></script>
    <script src="<?php echo base_url("js/foundation.min.js"); ?>"></script>
    <script src="<?php echo base_url("js/modernizr.js"); ?>"></script>
    <script src="<?php echo base_url("js/main.js"); ?>"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="//maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="//www.google.com/jsapi?key=AIzaSyDZfCFQOMzlWi6meIgIkFb5-LdKgBuGVFo"></script>
    <script src="<?php echo base_url("js/toastr.js"); ?>"></script>
</head>
<body>
	<div id="loadpage" style="width:100%;height:100%;position: absolute; background-color:rgba(255,255,255,0.8); visibility:hidden;z-index: 10;">
	    <center>
	        <br /><br />
	      	<img src="<?php echo base_url("img/ajax-loader.gif"); ?>">
	        <br /><br />
	        <h2 id="waitingtext"></h2>
	    </center>
	</div>

    <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url("/homepage"); ?>" style="color: white; font-family: Verdana; font-weight: bold;"><?php echo $header_honeynet?></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li id="listitem">
                        <a href="<?php echo site_url("/location"); ?>" ><?php echo $header_map?></a>
                    </li>
                    <li class="dropdown">
                        <a id="listitem" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $header_honeypots?><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php echo $honeylist ?>
                        </ul>
                    </li>
                    <li id="listitem">
                        <a href="<?php echo site_url("/history"); ?>/history" ><?php echo $header_history?></a>
                    </li>
                    <?php
                        $sess_array = $this -> session -> userdata('logged_in');
                        if ($sess_array['admin'] != 0) { ?>
                    <li id="listitem">
                        <a href="<?php echo site_url("/portal"); ?>" ><?php echo $header_portal?></a>
                    </li>
                    <?php } ?>
                    <?php
                        $sess_array = $this -> session -> userdata('logged_in');
                        if ($sess_array['admin'] != 0) { ?>
                    <li id="listitem">
                        <a href="<?php echo site_url("/admin"); ?>" ><?php echo $header_admin?></a>
                    </li>
                    <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li id="listitem">
                        <a href="<?php echo site_url("/homepage/logout"); ?>"><span class="sr-only">(current)</span><?php echo $header_logout?></a>
                    </li>
                    <li id="listitem">
                        <a href="<?php echo site_url("/homepage/settings"); ?>"  style="border: none;"><?php echo $header_settings?></a>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container -->
    </nav>
    <script>
        (function($){
                $(document).ready(function(){
                    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        $(this).parent().siblings().removeClass('open');
                        $(this).parent().toggleClass('open');
                    });
                });
            })(jQuery);
    </script>