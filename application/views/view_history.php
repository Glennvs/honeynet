<section>
    <title>
        <?php echo $history_titel?>
    </title>
    <div class="well">
        <h2><?php echo $history_titel?></h2>
        <div class="row">
            <div class="col-xs-12">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" onclick="lastWeek()" class="btn btn-default">
                        <?php echo $history_lastWeek?>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button id="month" onclick="lastTwo()" type="button" class="btn btn-default">
                        <?php echo $history_lastTwo?>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button id="three" onclick="lastThree()" type="button" class="btn btn-default">
                        <?php echo $history_lastThree?>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button id="six" onclick="lastMonth()" type="button" class="btn btn-default">
                        <?php echo $history_lastMonth?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="">
                <h3 id="data"></h3>
                <div id="columnchart" style="position: relative; top: 0px;"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    google.load("visualization", "1",
    {
        packages: ["corechart"]
    });

    /**
    * Shows the type and amount of attacks per date from the last month
    * @param none
    * @return none
    */
    function lastMonth()
    {
    	var result = <?php echo json_encode($month); ?> ;
    	if(result.length > 0)
    	{
         var time = [['Type', 'ssh', 'php', 'malware', { role: 'annotation' }]];
         var dataList = [];

         result = result.slice(-31);

    for(i = 0; i < result.length; i++)
    {
    dataList.push(result[i]);
    }

    for(i = 0; i < dataList.length; i++)
    {
    for(j = 1; j < dataList[i].length; j++)
    {
    	dataList[i][j] = parseInt(dataList[i][j]);
    }
    dataList[i].push("");
    time.push(dataList[i]);
    }

         var data = google.visualization.arrayToDataTable(time);
         var chartHeight = data.getNumberOfRows() * 100;
         var options = {
             backgroundColor: 'transparent',
             title: 'Last month',
             colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],
             height: chartHeight,
             chartArea: {top: 20, width: "80%", height: "80%" },
             fontSize: 14,
             bar: { groupWidth: '75%' },
         	isStacked: true
         };
         var chart = new google.visualization.BarChart(document.getElementById('columnchart'));
         document.getElementById('data').innerHTML = "";
         chart.draw(data, options);
    	}
    	else
    {
    document.getElementById('data').innerHTML = "No data available at the moment";
    document.getElementById('columnchart').innerHTML = "";
    }
    }

    /**
    * Shows the type and amount of attacks per date from the last three weeks
    * @param none
    * @return none
    */
    function lastThree()
    {
    	var result = <?php echo json_encode($three); ?> ;
    	if(result.length > 0)
    	{
         var time = [['Type', 'ssh', 'php', 'malware', { role: 'annotation' }]];
         var dataList = [];

         result = result.slice(-21);

    for(i = 0; i < result.length; i++)
    {
    dataList.push(result[i]);
    }

    for(i = 0; i < dataList.length; i++)
    {
    for(j = 1; j < dataList[i].length; j++)
    {
    	dataList[i][j] = parseInt(dataList[i][j]);
    }
    dataList[i].push("");
    time.push(dataList[i]);
    }

         var data = google.visualization.arrayToDataTable(time);
         var chartHeight = data.getNumberOfRows() * 100;
         var options = {
             backgroundColor: 'transparent',
             title: 'Last three weeks',
             colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],
             height: chartHeight,
             chartArea: {top: 20, width: "80%", height: "80%" },
             fontSize: 14,
             bar: { groupWidth: '75%' },
         	isStacked: true
         };
         var chart = new google.visualization.BarChart(document.getElementById('columnchart'));
         document.getElementById('data').innerHTML = "";
         chart.draw(data, options);
    	}
    	else
    {
    document.getElementById('data').innerHTML = "No data available at the moment";
    document.getElementById('columnchart').innerHTML = "";
    }
    }

    /**
    * Shows the type and amount of attacks per date from the last two weeks
    * @param none
    * @return none
    */
    function lastTwo()
    {
        var result = <?php echo json_encode($two); ?> ;
        if(result.length > 0)
    	{
         var time = [['Type', 'ssh', 'php', 'malware', { role: 'annotation' }]];
         var dataList = [];

         result = result.slice(-14);

    for(i = 0; i < result.length; i++)
    {
    dataList.push(result[i]);
    }

    for(i = 0; i < dataList.length; i++)
    {
    for(j = 1; j < dataList[i].length; j++)
    {
    	dataList[i][j] = parseInt(dataList[i][j]);
    }
    dataList[i].push("");
    time.push(dataList[i]);
    }

         var data = google.visualization.arrayToDataTable(time);
         var chartHeight = data.getNumberOfRows() * 100;
         var options = {
             backgroundColor: 'transparent',
             title: 'Last two weeks',
             colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],
             height: chartHeight,
             chartArea: {top: 20, width: "80%", height: "80%" },
             fontSize: 14,
             bar: { groupWidth: '75%' },
         	isStacked: true
         };
         var chart = new google.visualization.BarChart(document.getElementById('columnchart'));
         document.getElementById('data').innerHTML = "";
         chart.draw(data, options);
    }
    else
    {
    document.getElementById('data').innerHTML = "No data available at the moment";
    document.getElementById('columnchart').innerHTML = "";
    }
    }

    /**
    * Shows the type and amount of attacks per date from the last week
    * @param none
    * @return none
    */
    function lastWeek()
    {
        var result = <?php echo json_encode($week); ?>;
        if(result.length > 0)
    	{
         var time = [['Type', 'ssh', 'php', 'malware', { role: 'annotation' }]];
         var dataList = [];

         result = result.slice(-7);

    for(i = 0; i < result.length; i++)
    {
    dataList.push(result[i]);
    }

    for(i = 0; i < dataList.length; i++)
    {
    for(j = 1; j < dataList[i].length; j++)
    {
    	dataList[i][j] = parseInt(dataList[i][j]);
    }
    dataList[i].push("");
    time.push(dataList[i]);
    }

         var data = google.visualization.arrayToDataTable(time);
         var chartHeight = data.getNumberOfRows() * 100;
         var options = {
             backgroundColor: 'transparent',
             title: 'Last 7 days',
             colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],
             height: chartHeight,
             chartArea: {top: 20, width: "80%", height: "80%" },
             fontSize: 14,
             bar: { groupWidth: '75%' },
         	isStacked: true
         };
         var chart = new google.visualization.BarChart(document.getElementById('columnchart'));
         document.getElementById('data').innerHTML = "";
         chart.draw(data, options);
    }
    else
    {
    document.getElementById('data').innerHTML = "No data available at the moment";
    document.getElementById('columnchart').innerHTML = "";
    }
    }
</script>