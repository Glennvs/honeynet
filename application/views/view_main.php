<!doctype html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?php echo base_url("css/normalize.css"); ?>">
    <link href="<?php echo base_url("css/bootstrap.css"); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url("css/opmaak.css"); ?>">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php echo base_url("js/jquery.js"); ?>"></script>
    <script src="<?php echo base_url("js/foundation.min.js"); ?>"></script>
    <script src="<?php echo base_url("js/modernizr.js"); ?>"></script>
    <script src="<?php echo base_url("js/main.js"); ?>"></script>
    <title>Login</title>
</head>
<body>
    <div class="container">
        <div class="row vertical-offset-100" style="margin-top: 10%; ">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $login_titel?></h3>
                    </div>
                    <div class="panel-body">
                       																																																				 <?php echo validation_errors(); ?>
                        <?php echo form_open('verifylogin'); ?>
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="<?php echo $login_username?>" name="username" type="text" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="<?php echo $login_password?>" name="password" type="password" required>
                            </div>
                            <input id="btnLogin" class="btn btn-lg btn-success btn-block" type="submit" value="<?php echo $login_button?>">
                        </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url("js/fastclick.js"); ?>"></script>
    <script src="<?php echo base_url("js/bootstrap.min.js"); ?>"></script>
</body>
</html>