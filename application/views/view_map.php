<section>
    <title>
        <?php echo $map_titel?>
    </title>
    <div class="well">
        <h2><?php echo $map_titel?></h2>
        <div class="row">
            <div class="col-xs-12">
                <div id="map" style="width: 100%; height: 500px; border-radius: 10px; margin-bottom: 10px;"></div>
            </div>
        </div>
        <div id="whiterow" class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div id="geochart"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="col-xs-12">
                    <div id="donutchart"></div>
                </div>
                <div class="col-xs-12">
                    <div id="donutchartD"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        google.load("visualization", "1",
        {
            packages: ["corechart"]
        });
        google.load("visualization", "1",
        {
            packages: ["geochart"]
        });
        google.setOnLoadCallback(drawChart);

        /**
        * draws the chart so show all attacks in a country
        * @param none
        * @return none
        * @author Glenn Van Schil
        */
        function drawChart()
        {
            var countries = <?php echo json_encode($country, JSON_FORCE_OBJECT); ?> ;
            var types = <?php echo json_encode($type); ?> ;
            var cResult = [
                ['Countries', 'Attacks/Country']
            ];
            for (var k in countries)
            {
                if (countries.hasOwnProperty(k))
                {
                    cResult.push([k, countries[k]]);
                }
            }
            var data = google.visualization.arrayToDataTable(
                cResult
            );

            var options = {
                title: 'Attacks',
                colors: ['#455868'],
                backgroundColor: 'transparent',
                chartArea: {width: "100%", height: "100%" },
            };



            var chart = new google.visualization.GeoChart(document.getElementById('geochart'));
            var chartDetail = new google.visualization.PieChart(document.getElementById('donutchartD'));
            var chartDonut = new google.visualization.PieChart(document.getElementById('donutchart'));

            /**
        * draws the detailed data of the attacks on the selected country
        * @param none
        * @return none
        * @author Glenn Van Schil
        * @author Michiel Vanderlinden
        */
            function selectHandler()
            {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem)
                {
                    var value = data.getValue(selectedItem.row, 0);
                    var ssh;
                    var php;
                    var malware;
                    for (i = 0; i < types.length; i++)
                    {
                    	if (value == types[i][0])
                    	{
                    		if(types[i][1] == "ssh")
                    		{
                    			ssh = types[i][2];
                    		}
                    		else if(types[i][1] == "php")
                    		{
                    			php = types[i][2];
                    		}
                   			else if(types[i][1] == "malware")
                    		{
                    			malware = types[i][2];
                    		}
                    	}
                    }
                    var dataDetail = google.visualization.arrayToDataTable([
                            ['Attack types per country', 'Amount'],
                            ['SSH', parseInt(ssh)],
                            ['PHP', parseInt(php)],
                            ['Malware', parseInt(malware)]
                        	]);

                    var optionsDetail = {
                        title: 'Attack types per country',
                        pieHole: 0.45,
                        colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],

                        backgroundColor: 'transparent'
                    };

                    chartDetail.draw(dataDetail, optionsDetail);
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, options);

            var attacks = <?php echo json_encode($attacks); ?> ;
            var ssh;
            var php;
            var malware;
            for (i = 0; i < attacks.length; i++)
            {
                if (attacks[i][0] == "ssh")
                {
                    ssh = attacks[i][1];
                }
                else if (attacks[i][0] == "php")
                {
                    php = attacks[i][1];
                }
                else if (attacks[i][0] == "malware")
                {
                    malware = attacks[i][1];
                }
            }
            var dataDonut = google.visualization.arrayToDataTable([
                ['Type of Attack', 'Amount'],
                ['SSH', parseInt(ssh)],
                ['PHP', parseInt(php)],
                ['Malware', parseInt(malware)]
            ]);

            var optionsDonut = {
                title: 'Type of Attacks',
                pieHole: 0.45,
                colors: ['#455868', '#556B80', '#5C778C', '#627D92', '#6C8CA5', '#7292AB', '#7698B3', '#7D9FBA', '#82A3C2'],

                backgroundColor: 'transparent'
            };
            chartDonut.draw(dataDonut, optionsDonut)
            }
    </script>
    <script type="text/javascript">
        var result = <?php echo json_encode($map); ?> ;
        var locations = [];
        var ipList = [];
        var lat = [];
        var lon = [];

        for (i = 0; i < result.length; i++)
        {
            $.each(result[i], function(k, v)
            {
                if (!k.localeCompare("geo1"))
                {
                    lat.push(v)
                }
                else if (!k.localeCompare("geo2"))
                {
                    lon.push(v)
                }
                else if (!k.localeCompare("ip"))
                {
                    ipList.push(v);
                };
            });
        }

        for (i = 0; i < ipList.length; i++)
        {
            locations.push([ipList[i], lat[i], lon[i]])
        }

        showPosition(locations);

        /**
        * Show the attackers locations on a google map
        * @param geolocations (ip,lat, lon)
        * @return none
        * @author Glenn Van Schil
        * @author Michiel Vanderlinden
        */
        function showPosition(geoLocations)
        {
            var styles = [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#444444"
                }]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                {
                    "color": "#f2f2f2"
                }]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#052366"
                },
                {
                    "saturation": "-70"
                },
                {
                    "lightness": "85"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "lightness": "-53"
                },
                {
                    "weight": "1.00"
                },
                {
                    "gamma": "0.98"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.icon",
                "stylers": [
                {
                    "visibility": "simplified"
                }]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                },
                {
                    "visibility": "on"
                }]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                {
                    "saturation": "-18"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                {
                    "visibility": "off"
                }]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                {
                    "color": "#455868"
                },
                {
                    "visibility": "on"
                }]
            }]

            var map = new google.maps.Map(document.getElementById('map'),
            {
                zoom: 2,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            map.setOptions(
            {
                styles: styles
            });

            var infowindow = new google.maps.InfoWindow();

            var marker,
                i;

            for (i = 0; i < geoLocations.length; i++)
            {
                marker = new google.maps.Marker(
                {
                    position: new google.maps.LatLng(geoLocations[i][1], geoLocations[i][2]),
                    map: map,
                    icon: "<?php echo base_url("img/bee.png"); ?>"
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i)
                {
                    return function()
                    {
                        infowindow.setContent(geoLocations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));

                marker = new google.maps.Marker(
                {
                    position: new google.maps.LatLng(50.930031, 5.366853),
                    map: map,
                    icon: "<?php echo base_url("img/cegeka.png"); ?>"
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i)
                {
                    return function()
                    {
                        infowindow.setContent("Cegeka");
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
    </script>
</section>