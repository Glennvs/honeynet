<section>
    <title><?php echo $portal_titel?></title>
    <div class="well">
        <h2><?php echo $portal_titel?></h2>
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?php echo $portal_vm_name?></th>
                        <th><?php echo $portal_vm_power?></th>
                        <th><?php echo $portal_vm_reboot?></th>
                        <th><?php echo $portal_vm_delete?></th>
                        <th><?php echo $portal_vm_uptime?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $counter = 0;
                        for ($i = 0; $i < count($vmlist); $i=$i+2)
                        {
                        	if(strpos($power[$counter],'on') !== false)
                        	{
                        	$button = '<a onclick="loadingpage(\'turnoff\')" class="btn btn-success" href="'.site_url().'/portal/turnOff/'.$vmlist[$i].
                        	'"role="button" style="width:50px;">ON</a>';
                        	}
                        	else
                        	{
                        	$button = '<a onclick="loadingpage(\'turnon\')" class="btn btn-danger" href="'.site_url().'/portal/turnOn/'.$vmlist[$i].
                        	'"role="button" style="width:50px;">OFF</a>';
                        	}
                        	$buttondelete = '<a onclick="loadingpage(\'delete\')" class="btn btn-default" href="'.site_url().'/portal/deleteVm/'.$vmlist[$i].'/'.$vmlist[$i+1] . '"role="button">Delete</a>';
                        	$buttonreboot = '<a onclick="loadingpage(\'reboot\')" class="btn btn-default" href="'.site_url().'/portal/reboot/'.$vmlist[$i]. '" role="button">Reboot</a>';
                        ?>
                    <tr>
                        <td><?php echo $vmlist[$i+1]; ?></td>
                        <td><?php echo $button; ?></td>
                        <td><?php echo $buttonreboot; ?></td>
                        <td><?php echo $buttondelete; ?></td>
                        <td><?php echo $up[$counter]; ?></td>
                    </tr>
                    <?php
                        $counter++;
                        }
                        ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="well">
        <h2><?php echo $portal_add_vm?></h2>
        <div class="row">
            <form action="<?=site_url('portal/newServer')?>" method="post" class="form-horizontal">
           		<div class="form-group col-sm-offset-2 col-sm-10">
            		<select name="honeypot">
            			<option value="kippo">Kippo</option>
            			<option value="labrea">Labrea</option>            							
            		</select>
            	</div>
                <div class="form-group col-sm-offset-2 col-sm-10">
                    <input type="text" placeholder="Server name" name="name" onblur="this.value=removeSpaces(this.value);"/>
                </div>
                <div class="form-group col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-default" value="New Server" onclick="loadingpage('create')" name="newserver"/>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
var id = <?php echo json_encode($id); ?> ;
function loadingpage(text)
{

	if(text=='create'){
	document.getElementById("waitingtext").innerHTML="Creating server please wait...";

	}
	else if(text=='reboot'){
	document.getElementById("waitingtext").innerHTML="Rebooting server please wait...";

	}
	else if(text=='delete'){
	document.getElementById("waitingtext").innerHTML="Deleting server please wait...";

	}
	else if(text=='turnon'){
	document.getElementById("waitingtext").innerHTML="Turning on server please wait...";

	}
	else if(text=='turnoff'){
	document.getElementById("waitingtext").innerHTML="Turning off server please wait...";

	}
	document.getElementById("loadpage").style.visibility="visible";
}
$( document ).ready(function() {
   	if(id.Button=='turnOn')
	{
	toastr.info('Turned '+id.Name+' ON');
	}
	else if(id.Button=='turnOff')
	{
	toastr.info('Turned '+id.Name+' OFF');
	}
	else if(id.Button=='reboot')
	{
	toastr.info('Rebooted '+id.Name);
	}

});
</script>