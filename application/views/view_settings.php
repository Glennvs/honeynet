<section>
    <title><?php echo $settings_titel?></title>
    <div class="well">
    <h2><?php echo $settings_titel?></h2>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <caption><?php echo $settings_update?></caption>
        <?php echo validation_errors(); ?>
        <form action="<?=site_url('settings/username') ?>" method="post" class="form-horizontal">
            <div class="form-group col-sm-offset-2 col-sm-10">
                <input type="text" placeholder="Username" name="username" onblur="this.value=removeSpaces(this.value);" required/>
            </div>
            <div class="form-group col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-default" value="Update user" name="updateUser"/>
            </div>
        </form>
        <form action="<?=site_url('settings/password') ?>" method="post" class="form-horizontal">
            <div class="form-group col-sm-offset-2 col-sm-10">
                <input type="password" placeholder="Password" name="password" required/>
                <div/>
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="password" placeholder="Password confirmation" name="passconf" required/>
                    </div>
                    <div class="form-group col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-default" value="Update password" name="updatePass"/>
                    </div>
        </form>
        </div>
        </div>
    </div>
</section>